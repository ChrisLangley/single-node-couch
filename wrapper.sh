#!/bin/bash
tini -- /docker-entrypoint.sh /opt/couchdb/bin/couchdb &
COUCH=$!

#Capture exits
trap exit_script SIGINT

exit_script(){
  exit
}

sleep 3;
if [ "$COUCHDB_USER" ] && [ "$COUCHDB_PASSWORD" ]
then
  URL="http://$COUCHDB_USER:$COUCHDB_PASSWORD@127.0.0.1:5984"
else
  URL="http://127.0.0.1:5984"
fi

echo "INITING DATABASE"
curl -X PUT "$URL/_users"
curl -X PUT "$URL/_replicator"
curl -X PUT "$URL/_global_changes"
echo "DONE"
while [ -e /proc/$COUCH ]; do
    sleep 5;
done